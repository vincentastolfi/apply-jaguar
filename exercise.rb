#!/usr/bin/env ruby 
# Vincent Astolfi, 2014
class Exercise  
  def self.fiboseq(int)
  if int < 2
      int
  else
    fiboseq(int-1) + fiboseq(int-2)
  end
  end
  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
  thestr = str
  spltmrklr = thestr.split(" ")
  spltmrklr.map! { |mklr| (mklr.length<5 ? ""  : mklr) }
  spltmrklr.map!{ |punc| punc.gsub("?", '') }
  spltmrklr.map!{ |punc| punc.gsub(".", '') }
  spltmrklr.reject!{|s| s==""}
  	for themklr in spltmrklr
		if themklr===themklr.capitalize then
    		thestr.gsub!(themklr, 'Marklar')
		else
    		thestr.gsub!(themklr, 'marklar')
		end
  	end
  return str
  end
  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
  fibonth = fiboseq(nth)
  evnfibs = [0,1]
  begin
  evnfibs.push(evnfibs[-1]+evnfibs[-2])
  end while not evnfibs[-1]+evnfibs[-2]>=fibonth
  puts evnfibs.inject{ |sum, nth| nth%2==0 ? sum+nth : sum }
  evnfibs.delete_if &:odd?
  evnfibs.inject{|sums,x| sums + x }
  end
end
